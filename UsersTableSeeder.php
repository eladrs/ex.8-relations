<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name'=>'dekel',
                    'email'=>'dekel@dekel.com',
                    'password'=>'1234',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name'=>'elad',
                    'email'=>'elad@elad.com',
                    'password'=>'12345',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name'=>'dan',
                    'email'=>'dan@dan.com',
                    'password'=>'123456',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
            ]
            );
    }
}
