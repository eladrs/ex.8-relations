<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class='container'>
<br/><br/>
  <table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col"> book title</th>
      <th scope="col">author name</th>
    </tr>
  </thead>
  <tbody>
  @foreach($books as $book)
    <tr>
      <td> {{$book->id}}</td>
      <td>{{$book->title}}</td>
      <td>{{$book->author}}</td>
    </tr>
    @endforeach

  </tbody>
</table>
<div class ="container">
<div class="col-3  offset-4">
<a href="http://localhost/Homework/public/books/create" class=" btn btn-secondary">Add another books to your list</a>
</div>
</div>
</div>
